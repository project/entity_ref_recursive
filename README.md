This module provides an Entity Reference field display plugin.

Normally an entity reference field can render a link to the referenced 
entity. In many cases, it is useful to display the hierarchy of links 
that may be available allowing navigaation directly from an entity 
to one higher up in the hierarchy.

This hierarchical structure is made by selecting parent entities rather 
than selecting children. There are other modules that can build a similar 
relationship using children links but it tends to be more complex. 
Likewise, this module allows the hierarichial display to be added 
to any entity reference field.

A Viewfield can be used to list child entities with entity references 
to the parent.

Theory of Operation
-------------------
Assume a set of entities with a parent link. We start with an entity 
'A' that has five parents. That is unusual but it highlights what
the display structure will look like. Typically there will only be 
one or two values in the parent link field.  

    A -> B -> C -> D
    A -> E -> F
    A -> G -> F
    A -> H -> C -> D
    A -> I

Displays as:

    >> D
      >> C
        >> B
        >> H
    >> F
      >> E
      >> G 

The code handles circular references including an entity that
points to itself. The Maximum depth setting can limit the number of
levels displayed. The system assumes the parent field type is identical
but it does not care about the entity type.  

Installation
------------
Standard Drupal installation. No dependencies.

Operation
---------
The format is display option for any Entity Reference field. 
- Create or use view an existing entity reference field in any node. 
- View Manage display tab of the content file containing the field. 
- Change the Format of the field to "Recursive labels"
- Optional change the settings by click on the gear to the right


Settings
--------
The following settings are available for each control. 

- Label field: optional label field selection by machine name. Default is 'title' 
- Prefix:  text that goes before the link label. Initially "&amp;raquo; "
- Suffix:  text that goes after the link label
- Item class: list of classes added to the item class
- List class: list of classes added to the list class
- Maximum depth: specifies the maximum number of levels to present

Styles
------
The lists have a class of entity-ref-recursive-list.
The items in the list have a class of entity-ref-recursive-list__item. 
Additional styles can be included via the settings form. 
The default styling is minimal.
