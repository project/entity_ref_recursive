<?php

namespace Drupal\entity_ref_recursive\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_ref_recursive' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_ref_recursive_formatter",
 *   label = @Translation("Recursive labels"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityRefRecursiveFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
      'label_field' => '',
      'entity_prefix' => '&raquo; ',
      'entity_suffix' => '',
      'item_class' => '',
      'list_class' => '',
      'max_depth' => '0',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
      'label_field' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label field'),
        '#description' => $this->t(
          'This is the field to use as a label for the link. ' .
          'The default is "title" if this field is empty. ' .
          'This should be system name for a text field in the entity like "field_alt_title".'
        ),
        '#default_value' => $this->getSetting('label_field'),
      ],
      'entity_prefix' => [
        '#type' => 'textfield',
        '#title' => $this->t('Prefix'),
        '#description' => $this->t(
          'Displayed before the link. ' .
          'The field can include HTML code.' .
          'The typical value is &amp;raquo; (&raquo;) followed by a space. ' .
          'The field supports token replacement.'),
        '#default_value' => $this->getSetting('entity_prefix'),
      ],
      'entity_suffix' => [
        '#type' => 'textfield',
        '#title' => $this->t('Suffix'),
        '#description' => $this->t(
          'Displayed after the link. ' .
          'The field supports token replacement.'),
        '#default_value' => $this->getSetting('entity_suffix'),
      ],
      'item_class' => [
        '#type' => 'textfield',
        '#title' => $this->t('Item class'),
        '#description' => $this->t(
          'Added to class list that include entity-ref-recursive-list__item. ' .
          'The field supports token replacement.'),
        '#default_value' => $this->getSetting('item_class'),
      ],
      'list_class' => [
        '#type' => 'textfield',
        '#title' => $this->t('List class'),
        '#description' => $this->t(
          'Added to class list that include entity-ref-recursive-list. ' .
          'The field supports token replacement.'),
        '#default_value' => $this->getSetting('list_class'),
      ],
      'max_depth' => [
        '#type' => 'select',
        '#title' => $this->t('Maximum depth'),
        '#description' => $this->t('Allows limiting of the number of levels presented'),
        '#options' => [
          0 => $this->t('Unlimited'),
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
          7 => '7',
          8 => '8',
          9 => '9',
        ],
        '#default_value' => $this->getSetting('max_depth'),
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Display entity references recursively');
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $root = [];
    $entity = [];
    $children = [];
    $max_depth = $this->getSetting('max_depth');
    if (0 == $max_depth) {
      // Essentially unlimited.
      $max_depth = 100000;
    }
    $this->processParents(0, $items, $root, $children, $entity, $max_depth);

    $result = $this->renderItems($root, $children, $entity);

    return $result ? [$result] : [];
  }

  /**
   * Fill arrays with entity tree information.
   *
   * @param int my_id
   *   entity ID
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Field item list.
   * @param array &$root
   *   List of root IDs.
   * @param array &$children
   *   List of lists of children IDs, key is parent ID.
   * @param array &$entity
   *   List of entity objects, key is entity ID.
   * @param int $max_depth
   *   Maximum depth, 0 = unlimited.
   */
  protected function processParents($my_id, $items, &$root, &$children, &$entity, $max_depth) {
    if (0 == $max_depth || $items->isEmpty()) {
      $root[] = $my_id;
    }
    else {
      $field_name = $items->getFieldDefinition()->getName();

      foreach ($items as $item) {
        $id = $item->getValue()['target_id'];
        $children[$id][] = $my_id;

        // Add an entity only once. This eliminates issues with circular lists.
        if (!isset($entity[$id])) {
          $entity[$id] = \Drupal::entityTypeManager()->getStorage('node')->load($id);
          $entity_items = $entity[$id]->get($field_name);
          $this->processParents($id, $entity_items, $root, $children, $entity, $max_depth - 1);
        }
      }
    }
  }

  /**
   * Generate the render array based on array tree information.
   *
   * @param array &$root
   *   List of root IDs.
   * @param array &$children
   *   List of lists of children IDs, key is parent ID.
   * @param array &$entity
   *   List of entity objects, key is entity ID.
   *
   * @return array
   *   renderable array
   */
  protected function renderItems(&$root, &$children, &$entity) {
    $result = [];
    $settings = $this->getSettings();
    $token = \Drupal::token();

    // Need at least one non-zero entry.
    if ($root && $root[0]) {
      $result['#theme'] = 'item_list';
      $result['#attributes']['class'][] =
        'entity-ref-recursive-list ' .
        $token->replace($settings['list_class']);
      $result['#attached']['library'][] = 'entity_ref_recursive/default';
      $items = [];
      foreach ($root as $id) {
        if ($id) {
          // Get link label, default is entity's title if field is missing.
          $label = ($settings['label_field'] && $entity[$id]->getFieldDefinition($settings['label_field'])) ?
                   $entity[$id]->get($settings['label_field'])->value :
                   '';
          if (!$label) {
            $label = $entity[$id]->get('title')->value;
          }
          $item['#markup'] =
            $token->replace($settings['entity_prefix']) .
            $entity[$id]->toLink($label ? $label : NULL)->toString() .
            $token->replace($settings['entity_suffix']);
          $item['#wrapper_attributes']['class'][] =
            'entity-ref-recursive-list__item ' .
            $token->replace($settings['item_class']);
          $my_list = $this->renderItems($children[$id], $children, $entity);
          if ($my_list) {
            $item[] = $my_list;
          }
          $items[] = $item;
        }
      }
      $result['#items'] = $items;
    }
    return $result;
  }

}
